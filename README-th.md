[For English, click here](./README.md)

# Icebomb

เป็น project REST server  เขียนโดย Go

## คุณสมบัติ

ตัวอย่างเหล่านี้ สาธิต:

* ระบบสมาชิก
* ส่งเมล
* ลบ records ที่ไม่ได้ใช้แล้ว, สามารถจะกำหนดค่าได้
* อัพโหลด profile icon
* เปลี่ยนภาษา: อังกฤษ, ไทย
* เปลี่ยนธีมสี
* เข้าสู่ระบบโดย Google และ LINE, บน web เท่านั้น

## Screenshots

รูปภาพหน้าจอ อยู่ใน "%SOURCE%/samples.icebomb/screenshots"

## การติดตั้ง

ถ้าคุณยังไม่ได้ติดตั้ง โปรแกรมสำหรับพัฒนา ติดตั้งได้แล้ว:

* [Go](https://go.dev), สำหรับ REST server
* [Vue](https://vuejs.org) และ [web browser](https://www.google.com/chrome), สำหรับ web client
* [Postman](https://www.postman.com), สำหรับทดสอบ server

## Run Server

Source code สำหรับ REST server อยู่ใน "samples.icebomb/icebomber" ใช้ [GORM](https://gorm.io) ในการเข้าถึง database

เปลี่ยน folder:

	cd %SOURCE%/samples.icebomb/icebomber

Run:

	go run .

สำหรับการ run ครั้งแรก, ใช้โปรแกรม REST client เช่น: Postman, โปรด run:

	POST http://localhost:6600/api/setup

โปรแกรมจะสร้างสมาชิก "admin@your.diy" กับรหัสผ่าน "admin"

## Run ทดสอบ Server

ถ้าคุณต้องการทดสอบ server เปิดโปรแกรม Postman และ import scripts "icebomber.postman_collection" และ "icebomber.postman_environment" ใน folder "%SOURCE%/samples.icebomb/icebomber"  อย่าลืม set Environments เป็น "icebomber"

## สร้างเอกสาร Server

ถ้าคุณต้องการสร้างเอกสารสำหรับ REST server  พิมพ์:

	cd %SOURCE%/samples.icebomb/icebomber

	godoc -http=:6060

และเปิด browser สำหรับ URL:

	http://localhost:6060

เบอร์ port สามารถเปลี่ยนได้

## Run Web/App

Source code สำหรับ web clients อยู่ใน "samples.icebomb/icewebber" (ไม่มี UI) หรือ "samples.icebomb/iceweaver" (ใช้ bootstrap 3).

เปลี่ยน folder:

	cd %SOURCE%/samples.icebomb/icewebber
	-- or --
	cd %SOURCE%/samples.icebomb/iceweaver

ติดตั้ง, ทำครั้งเดียวเท่านั้น:

	npm i

Run:

	npm start

สุดท้าย, เปิด browser และใส่ URL:

	http://localhost:8080

โปรดระลึกว่า ในเวลาที่เขียนโปรแกรมตัวนี้ bootstrap Vue 3 อยู่ในสถานะ beta และอาจมี components ที่ยังไม่ได้นำมาใส่

เนื่องจาก REST server มีความ compatible กับ [Coldfire project](https://gitlab.com/taworn.ta7/samples.coldfire) คุณสามารถใช้ทั้ง Flutter app และ web clients ใน project Coldfire ได้ทันที  Download [Coldfire](https://gitlab.com/taworn.ta7/samples.coldfire) และเปิด README ดูในส่วน Run App หรือไม่ก็ Run Web

## การกำหนดค่า

มีไฟล์ชื่อ "config.override.yaml" อยู่ในต้นทาง folder  มันเก็บการกำหนดค่าและสามารถเปลี่ยนได้

รายการกำหนดค่า:

log:
* daysToKeepDbLogs: จำนวนวันที่จะเก็บค่าเก่า database log
* daysToKeepSignUps: จำนวนวันที่จะเก็บค่าเก่า สำหรับ records ในตาราง member_signup
* daysToKeepResets: จำนวนวันที่จะเก็บค่าเก่า สำหรับ records ในตาราง member_reset

db:
* use: sqlite หรือ mysql, ผู้เขียนคิดว่า น่าจะมี BUG ใน sqlite
* host: เครื่องที่ต่อ database
* port: port ที่ใช้
* user: user ที่ใช้
* pass: รหัสผ่านสำหรับเข้า database

mail:
* host: เครื่องที่ต่อ สำหรับส่งเมล
* port: port ที่ใช้
* user: user ที่ใช้
* pass: รหัสผ่านสำหรับเข้าเมล

authen:
* timeout: เวลาก่อนที่ session จะหมดอายุ หน่วยเป็น มิลลิวินาที

profile:
* iconFileLimit: จำนวน bytes ที่จะจำกัด ตอนอัพโหลด profile icon

ถ้าคุณใช้ MySQL อย่าลืม สร้าง database เปล่า ชื่อ "icebomber"

ผมใช้ [ethereal](https://ethereal.email) ซึ่งเป็น mail service ปลอม สำหรับทดสอบ
และบางครั้ง ชื่อผู้ใช้กับรหัสผ่านจะหมดอายุ และคุณต้องสร้างขึ้นมาใหม่ ไม่เสียค่าใช้จ่าย

## เอกสารสำหรับการเขียนโปรแกรม

เอกสารเพิ่มเติม:

* [APIs ทั้งหมด](./doc/APIs.md)
* [การจัดการ Error สำหรับ Client](./doc/Error%20Handling%20for%20Client.md)
* [การจัดเรียงลำดับ](./doc/Sort%20Ordering.md)

## สุดท้าย

ขอโทษครับ แต่ผมไม่เก่งภาษาอังกฤษ T_T
