import axios, { AxiosError } from 'axios'

/**
 * Client connection manager.
 */
export class Client {

	/**
	 * Constructor.
	 */
	constructor(baseUrl, baseStaticUrl) {
		this.baseUrl = baseUrl
		if (!this.baseUrl.endsWith('/')) this.baseUrl += '/'
		this.baseStaticUrl = baseStaticUrl
		if (!this.baseStaticUrl.endsWith('/')) this.baseStaticUrl += '/'
	}

	// ----------------------------------------------------------------------

	/**
	 * Server base URL.
	 */
	baseUrl = ''

	/**
	 * Static server base URL.
	 */
	baseStaticUrl = ''

	// ----------------------------------------------------------------------

	/**
	 * Concats base URL with address.
	 */
	url(address) {
		return `${this.baseUrl}${address}`
	}

	// ----------------------------------------------------------------------

	/**
	 * Authen token.
	 */
	token = ''

	// ----------------------------------------------------------------------

	/**
	 * Gets default headers.
	 */
	defaultHeaders(token) {
		const t = token || this.token
		return {
			'Content-Type': 'application/json;charset=utf-8',
			'Authorization': `Bearer ${t}`,
		}
	}

	/**
	 * Gets multi-part headers.
	 */
	formDataHeaders(token) {
		const t = token || this.token
		return {
			'Content-Type': 'multipart/form-data',
			'Authorization': `Bearer ${t}`,
		}
	}

	// ----------------------------------------------------------------------

	/**
	 * Calls HTTP.
	 */
	async call(url, options) {
		// log before send
		let method
		if (options && options.method)
			method = options.method
		else
			method = 'GET'
		console.log(`${method} ${this.baseUrl + url}`)

		// fetches
		try {
			const res = await axios(this.baseUrl + url, options)
			console.log(`call result: ${res.status} ${res.statusText} ${JSON.stringify(res.data, null, 2)}`)
			return {
				ok: res.status === 200 || res.status === 201,
				status: res.status,
				json: res.data,
				res,
			}
		}
		catch (ex) {
			const err = ex
			if (err) {
				if (err.response) {
					const res = err.response
					const ret = {
						ok: res.status === 200 || res.status === 201,
						status: res.status,
						json: res.data,
						res,
					}
					console.log(`call error: ${JSON.stringify(ret, null, 2)}`)
					return ret
				}
				else {
					console.log(`call error: ${JSON.stringify(err, null, 2)}`)
					return null
				}
			}
			console.log(`call error: ${ex}`)
			return null
		}
	}

}
