// Package tasks for icebomber.
package tasks

import (
	"time"
)

func BackgroundTasks() {
	tasks()

	ticker := time.NewTicker(8 * time.Hour)
	for range ticker.C {
		tasks()
	}
}

func tasks() {
	deleteUnusedRecords()
}
