package tasks

import (
	"icebomb/icebomber/config"
	"icebomb/icebomber/dbi"
	"icebomb/icebomber/logger"
	"time"
)

func deleteUnusedRecords() {
	conf := config.Get()
	deleteUnusedLoggingRecords(conf.DaysToKeepDbLogs)
	deleteUnusedMemberSignUpRecords(conf.DaysToKeepSignUps)
	deleteUnusedMemberResetRecords(conf.DaysToKeepResets)
}

// ----------------------------------------------------------------------

func deleteUnusedLoggingRecords(daysToKeep int) {
	log := logger.Get().Logger
	db := dbi.Get().Db

	// checks before execute, days to keep must keep at least 0 (today)
	if daysToKeep < 0 {
		return
	}

	// computes date range
	now := time.Now()
	end := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.Local)
	begin := end.AddDate(0, 0, -daysToKeep)

	// cleanup old logging table
	log.Printf("database logs older than %v will be delete!", begin)
	db.Unscoped().Where("created_at < ?", begin).Delete(&dbi.Logging{})
}

// ----------------------------------------------------------------------

func deleteUnusedMemberSignUpRecords(daysToKeep int) {
	log := logger.Get().Logger
	db := dbi.Get().Db

	// checks before execute, days to keep must keep at least 0 (today)
	if daysToKeep < 0 {
		return
	}

	// computes date range
	now := time.Now()
	end := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.Local)
	begin := end.AddDate(0, 0, -daysToKeep)

	// cleanup old member_signup table
	log.Printf("sign-ups data older than %v will be delete!", begin)
	db.Unscoped().Where("created_at < ?", begin).Delete(&dbi.MemberSignUp{})
}

// ----------------------------------------------------------------------

func deleteUnusedMemberResetRecords(daysToKeep int) {
	log := logger.Get().Logger
	db := dbi.Get().Db

	// checks before execute, days to keep must keep at least 0 (today)
	if daysToKeep < 0 {
		return
	}

	// computes date range
	now := time.Now()
	end := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.Local)
	begin := end.AddDate(0, 0, -daysToKeep)

	// cleanup old member_reset table
	log.Printf("resets data older than %v will be delete!", begin)
	db.Unscoped().Where("created_at < ?", begin).Delete(&dbi.MemberReset{})
}
