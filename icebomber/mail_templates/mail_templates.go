package mail_templates

import (
	"html/template"
)

// Mail templates structure data type.
type MailTemplatesType struct {
	SignUp         *template.Template
	Reset          *template.Template
	ResetConfirmed *template.Template
}

var mailTemplate *MailTemplatesType = &MailTemplatesType{
	SignUp:         nil,
	Reset:          nil,
	ResetConfirmed: nil,
}

// Retrieves the mail templates.
func Get() *MailTemplatesType {
	return mailTemplate
}
