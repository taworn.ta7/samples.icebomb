package mail_templates

import (
	"bytes"
	"html/template"
)

// Reset mail template.
type ResetTemplate struct {
	Email        string
	URL          string
	ConfirmToken string
}

// Uses "reset" mail template.
func Reset(value *ResetTemplate) string {
	//log := logger.Get().Logger

	if mailTemplate.Reset == nil {
		mailTemplate.Reset = createTemplateReset()
	}
	t := mailTemplate.Reset

	var buffer bytes.Buffer
	t.Execute(&buffer, value)
	body := buffer.String()

	return body
}

func createTemplateReset() *template.Template {
	t, err := template.New("reset").Parse(`
<html>

<head>
	<title>Reset Password Confirmation</title>
</head>

<body>
	<p>
		You want to reset password of member {{.Email}}, right?
	</p>

	<p>
		If yes, click to reset password: <a href="{{.URL}}">{{.ConfirmToken}}</a><br />
		Otherwise, ignore this email.<br />
	</p>
</body>

</html>
`)
	if err != nil {
		panic(err)
	}
	return t
}
