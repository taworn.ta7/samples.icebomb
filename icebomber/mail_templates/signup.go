package mail_templates

import (
	"bytes"
	"html/template"
)

// Sign-up mail template.
type SignUpTemplate struct {
	Email        string
	URL          string
	ConfirmToken string
}

// Uses "signup" mail template.
func SignUp(value *SignUpTemplate) string {
	//log := logger.Get().Logger

	if mailTemplate.SignUp == nil {
		mailTemplate.SignUp = createTemplateSignUp()
	}
	t := mailTemplate.SignUp

	var buffer bytes.Buffer
	t.Execute(&buffer, value)
	body := buffer.String()

	return body
}

func createTemplateSignUp() *template.Template {
	t, err := template.New("signup").Parse(`
<html>

<head>
	<title>Sign-up Confirmation</title>
</head>

<body>
	<p>
		Thank you, {{.Email}}.
	</p>

	<p>
		Your sign-up code: <a href="{{.URL}}">{{.ConfirmToken}}</a>
	</p>
</body>

</html>	
`)
	if err != nil {
		panic(err)
	}
	return t
}
