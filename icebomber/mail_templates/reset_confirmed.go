package mail_templates

import (
	"bytes"
	"html/template"
)

// Reset confirmed mail template.
type ResetConfirmedTemplate struct {
	Email    string
	Password string
}

// Uses "reset_confirmed" mail template.
func ResetConfirmed(value *ResetConfirmedTemplate) string {
	//log := logger.Get().Logger

	if mailTemplate.ResetConfirmed == nil {
		mailTemplate.ResetConfirmed = createTemplateResetConfirmed()
	}
	t := mailTemplate.ResetConfirmed

	var buffer bytes.Buffer
	t.Execute(&buffer, value)
	body := buffer.String()

	return body
}

func createTemplateResetConfirmed() *template.Template {
	t, err := template.New("reset_confirmed").Parse(`
<html>

<head>
	<title>Reset Password</title>
</head>

<body>
	<p>
		Your sign-in password of {{.Email}} has been reset.
	</p>

	<p>
		Your new password: {{.Password}}
	</p>

	<p>
		After sign-in using new password, please change your password again.
	</p>
</body>

</html>
`)
	if err != nil {
		panic(err)
	}
	return t
}
