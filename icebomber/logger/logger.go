// Package custom logger for icebomber.
package logger

import (
	"io"
	"log"
	"os"

	"icebomb/icebomber/config"

	"github.com/davecgh/go-spew/spew"
	"gopkg.in/natefinch/lumberjack.v2"
)

// Logger structure data type.
type LoggerType struct {
	Logger *log.Logger
	Writer *io.Writer
	Spew   *spew.ConfigState
}

var logger *LoggerType

// Retrieves the logger.
func Get() *LoggerType {
	return logger
}

// ----------------------------------------------------------------------

// Dumps a variable.
func Dump(a ...interface{}) string {
	return logger.Spew.Sdump(a)
}

// ----------------------------------------------------------------------

// Setup the logger.
func Setup() *LoggerType {
	config := config.Get()

	var ws []io.Writer

	// opens file log
	if config != nil {
		ws = append(ws, &lumberjack.Logger{
			Filename:   config.LogFileName,
			MaxSize:    config.LogMaxSize,
			MaxBackups: config.LogMaxBackups,
			MaxAge:     config.LogMaxAge,
			Compress:   config.LogCompress,
		})
	} else {
		ws = append(ws, &lumberjack.Logger{
			Filename:   "~log/icebomber.log",
			MaxSize:    500,
			MaxBackups: 2,
			MaxAge:     10,
			Compress:   false,
		})
	}

	// opens console log
	ws = append(ws, os.Stdout)

	// merges multi-writers
	w := io.MultiWriter(ws...)

	// creates new log
	l := log.Default()
	l.SetOutput(w)
	l.SetFlags(log.Ldate | log.Ltime | log.Lmicroseconds | log.Lshortfile)

	// creates spew config
	spew := spew.NewDefaultConfig()
	spew.Indent = "  "

	log := &LoggerType{
		Logger: l,
		Writer: &w,
		Spew:   spew,
	}
	logger = log
	return logger
}
