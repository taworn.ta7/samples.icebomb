package main

import (
	"fmt"
	"icebomb/icebomber/config"
	"icebomb/icebomber/dbi"
	"icebomb/icebomber/helpers"
	"icebomb/icebomber/logger"
	"icebomb/icebomber/middlewares"
	"icebomb/icebomber/routes"
	"icebomb/icebomber/routes/admin"
	"icebomb/icebomber/routes/authenx"
	"icebomb/icebomber/routes/members"
	"icebomb/icebomber/routes/profile"
	"icebomb/icebomber/routes/settings"
	"icebomb/icebomber/tasks"
	"runtime"

	"github.com/gin-gonic/gin"

	"github.com/mattn/go-colorable"
)

func main() {
	// setup
	conf := config.Setup()
	logger.Setup()
	dbi.Setup()
	helpers.Setup()

	// prints startup information
	fmt.Printf("%v, version %v\n", "Icebomber REST webserver", conf.AppVersion)
	fmt.Printf("\n")
	fmt.Printf("%v\n", conf.Log())

	// gin setup
	gin.ForceConsoleColor()
	//gin.DisableConsoleColor()
	gin.DefaultWriter = *logger.Get().Writer
	if runtime.GOOS == "windows" {
		gin.DefaultWriter = colorable.NewColorableStdout()
	}

	// router setup
	router := gin.Default()
	router.Use(middlewares.CORSMiddleware())
	router.Use(middlewares.Logging())
	router.Use(middlewares.ErrorHandler())

	// set a lower memory limit for multipart forms (default is 32 MiB)
	router.MaxMultipartMemory = 10 << 20 // 10 MiB

	// without authentication routes
	normal := router.Group("/api")
	normal.GET("/about", routes.About, middlewares.Dump(true, true))
	normal.GET("/config", routes.Config)
	normal.POST("/setup", routes.Setup)
	normal.POST("/members/signup", members.SignUp)
	normal.GET("/members/signup/confirm", members.ConfirmSignUp)
	normal.POST("/members/request-reset", members.RequestReset)
	normal.GET("/members/request-reset/reset-password", members.ResetPassword)
	normal.GET("/members", members.List)
	normal.GET("/members/:email", members.Get)
	normal.GET("/members/:email/icon", members.GetIcon)
	normal.PUT("/authen/signin", routes.SignIn)

	// with authentication routes
	authen := router.Group("/api")
	authen.Use(middlewares.Authentication(middlewares.CheckAuthen))
	authen.PUT("/authen/signout", routes.SignOut)
	authen.GET("/authen/check", routes.Check)
	authen.POST("/profile/icon", profile.Upload)
	authen.GET("/profile/icon", profile.View)
	authen.DELETE("/profile/icon", profile.Delete)
	authen.PUT("/profile/name", profile.ChangeName)
	authen.PUT("/profile/password", profile.ChangePassword)
	authen.PUT("/settings/change", settings.Change)
	authen.PUT("/settings/:key/:value", settings.Set)
	authen.GET("/settings/:key", settings.Get)
	authen.GET("/settings", settings.GetAll)
	authen.PUT("/settings", settings.Reset)

	// with admin authentication routes
	authenAdmin := router.Group("/api/admin")
	authenAdmin.Use(middlewares.Authentication(middlewares.CheckAuthenAdmin))
	authenAdmin.PUT("/members/authorize", admin.AuthorizedMember)
	authenAdmin.PUT("/members/disable/:email", admin.DisabledMember)

	// external sign-in
	external := router.Group("/api/authenx")
	external.GET("/google", authenx.GoogleExternalSignIn)
	external.GET("/line", authenx.LineExternalSignIn)

	// prepares timer
	go tasks.BackgroundTasks()

	// run server
	router.Run(fmt.Sprintf(":%v", conf.HttpPort))
}
