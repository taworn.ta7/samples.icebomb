package helpers

import (
	"fmt"
	"regexp"
	"strings"
)

// Pagination data.
type Pagination struct {
	Page        int   `json:"page"`
	Offset      int   `json:"offset"`
	RowsPerPage int   `json:"rowsPerPage"`
	Count       int64 `json:"count"`
	PageIndex   int   `json:"pageIndex"`
	PageCount   int   `json:"pageCount"`
	PageStart   int   `json:"pageStart"`
	PageStop    int   `json:"pageStop"`
	PageSize    int   `json:"pageSize"`
}

// Gets pagination from page, rows per page and count.
func GetPagination(page int, rowsPerPage int, count int64) *Pagination {
	offset := page * rowsPerPage
	pageCount := count / int64(rowsPerPage)
	if count%int64(rowsPerPage) > 0 {
		pageCount++
	}

	var pageSize int64
	if int64(page) < pageCount-1 {
		pageSize = int64(rowsPerPage)
	} else {
		if int64(page) >= pageCount {
			pageSize = 0
		} else {
			pageSize = count - int64(offset)
		}

	}

	return &Pagination{
		Page:        page,
		Offset:      offset,
		RowsPerPage: rowsPerPage,
		Count:       count,
		PageIndex:   page,
		PageCount:   int(pageCount),
		PageStart:   offset,
		PageStop:    offset + int(pageSize),
		PageSize:    int(pageSize),
	}
}

// Gets ordering data.
func GetOrderingData(sortDict *map[string]string, order string) *map[string]string {
	result := make(map[string]string, 10)

	regex := regexp.MustCompile(`([a-zA-Z0-9_]+)([\+\-]?)`)
	list := regex.FindAllStringSubmatch(order, -1)
	for i := 0; i < len(list); i++ {
		item := list[i]
		field := item[1]
		if value, ok := (*sortDict)[field]; ok {
			var dir string
			if item[2] == "-" {
				dir = "DESC"
			} else {
				dir = "ASC"
			}
			result[value] = dir
		}
	}

	return &result
}

// Gets ordering data as string.
func GetOrderingAsString(sortDict *map[string]string, order string) string {
	data := GetOrderingData(sortDict, order)
	list := make([]string, len(*data))
	i := 0
	for k, v := range *data {
		list[i] = fmt.Sprintf("%v %v", k, v)
		i++
	}
	return strings.Join(list, ", ")
}
