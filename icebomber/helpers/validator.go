package helpers

import (
	"log"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

func Validate(c *gin.Context, value interface{}) bool {
	id, exists := c.Get("id")
	if !exists {
		id = "<no request id>"
	}

	validate := validator.New()
	if err := validate.Struct(value); err != nil {
		// this check is only needed when your code could produce
		// an invalid value for validation such as interface with nil
		// value most including myself do not usually have code like this.
		if _, ok := err.(*validator.InvalidValidationError); ok {
			log.Printf("%v; %v", id, err)
		}

		for _, err := range err.(validator.ValidationErrors) {
			log.Printf("%v; %v", id, err)
		}

		// from here you can create your own error messages in whatever language you wish
		c.Error(ErrValidationFailed)
		return false
	}

	return true
}
