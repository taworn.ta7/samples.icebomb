package helpers

import (
	"strconv"
)

func AtoiWithDefault(value string, def int) int {
	i, e := strconv.Atoi(value)
	if e != nil {
		return def
	}
	return i
}
