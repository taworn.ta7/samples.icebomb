package helpers

import (
	"icebomb/icebomber/logger"
	"os"
	"testing"
)

var sortDict = &map[string]string{
	"email":   "email",
	"name":    "name",
	"created": "created",
	"updated": "updated",
}

func TestOrderingData_0(t *testing.T) {
	orders := GetOrderingData(sortDict, "")
	if len(*orders) != 0 {
		t.Errorf("orders must be empty")
	}
}

// ----------------------------------------------------------------------

func TestOrderingData_1(t *testing.T) {
	orders := GetOrderingData(sortDict, "email-")
	if len(*orders) != 1 {
		t.Errorf("orders must be 1 entry")
		return
	}
	value, ok := (*orders)["email"]
	if !ok {
		t.Errorf("orders must have field: email")
		return
	}
	if value != "DESC" {
		t.Errorf("orders field email must be DESC")
		return
	}
}

// ----------------------------------------------------------------------

func TestOrderingData_2(t *testing.T) {
	orders := GetOrderingData(sortDict, "email- , name+")
	if len(*orders) != 2 {
		t.Errorf("orders must be 2 entries")
		return
	}

	{
		value, ok := (*orders)["email"]
		if !ok {
			t.Errorf("orders must have field: email")
			return
		}
		if value != "DESC" {
			t.Errorf("orders field email must be DESC")
			return
		}
	}

	{
		value, ok := (*orders)["name"]
		if !ok {
			t.Errorf("orders must have field: name")
			return
		}
		if value != "ASC" {
			t.Errorf("orders field name must be ASC")
			return
		}
	}
}

// ----------------------------------------------------------------------

func TestOrderingData_3(t *testing.T) {
	orders := GetOrderingData(sortDict, "email- , name+	created")
	if len(*orders) != 3 {
		t.Errorf("orders must be 3 entries")
		return
	}

	{
		value, ok := (*orders)["email"]
		if !ok {
			t.Errorf("orders must have field: email")
			return
		}
		if value != "DESC" {
			t.Errorf("orders field email must be DESC")
			return
		}
	}

	{
		value, ok := (*orders)["name"]
		if !ok {
			t.Errorf("orders must have field: name")
			return
		}
		if value != "ASC" {
			t.Errorf("orders field name must be ASC")
			return
		}
	}

	{
		value, ok := (*orders)["created"]
		if !ok {
			t.Errorf("orders must have field: created")
			return
		}
		if value != "ASC" {
			t.Errorf("orders field created must be ASC")
			return
		}
	}
}

// ----------------------------------------------------------------------

func setup() {
	//config.Setup()
	logger.Setup()
}

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	//shutdown()
	os.Exit(code)
}
