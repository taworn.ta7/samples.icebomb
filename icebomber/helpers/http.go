package helpers

import (
	"github.com/gin-gonic/gin"
)

func Protocol(c *gin.Context) string {
	url := "http://"
	if c.Request.TLS != nil {
		url = "https://"
	}
	return url
}
