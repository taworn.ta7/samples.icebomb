// Package helpers for icebomber.
package helpers

import (
	"math/rand"
	"time"
)

// Setup the helpers.
func Setup() {
	rand.Seed(time.Now().UnixNano())
}
