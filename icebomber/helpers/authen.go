package helpers

import (
	"icebomb/icebomber/config"
	"icebomb/icebomber/dbi"
	"math/rand"
	"time"

	"github.com/GehirnInc/crypt"
	_ "github.com/GehirnInc/crypt/sha256_crypt"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
)

var salt = "$5$salt"
var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

// Sets password and returns structure salt and hash.
func SetPassword(password string) (string, string) {
	crypt := crypt.SHA256.New()
	hash, _ := crypt.Generate([]byte(password), []byte(salt))
	return hash, salt
}

// Validates password with salt and hash.
func ValidatePassword(password string, hash string) bool {
	crypt := crypt.SHA256.New()
	err := crypt.Verify(hash, []byte(password))
	return err == nil
}

// Generates secret string.
func GenerateSecret(length int) string {
	b := make([]rune, length)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

// ----------------------------------------------------------------------

// Creates or updates token in member.  The member structure must include credential.
func NewOrUpdateToken(member *dbi.Member) string {
	conf := config.Get()
	var token string
	n := time.Now()
	now := &n
	expire := now.Add(time.Duration(conf.AuthenTimeout * int64(time.Millisecond)))
	if member.Credential.Token != nil && member.Expire != nil && member.Expire.After(*now) {
		// updates expiry sign-in
		member.Expire = &expire
		token = *member.Credential.Token
	} else {
		// signs token
		claims := &jwt.StandardClaims{
			Id:      member.ID,
			Subject: "Icebomb",
		}
		jwt := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
		secret := GenerateSecret(16)
		token, _ = jwt.SignedString([]byte(secret))

		// updates member
		member.Begin = now
		member.End = nil
		member.Expire = &expire
		member.Credential.Token = &token
	}
	return token
}

// ----------------------------------------------------------------------

// Creates new member which removes credential data.
func ReduceMemberData(member dbi.Member) *dbi.Member {
	return &dbi.Member{
		ID:        member.ID,
		Email:     member.Email,
		Role:      member.Role,
		Locale:    member.Locale,
		Name:      member.Name,
		Disabled:  member.Disabled,
		Resigned:  member.Resigned,
		Begin:     member.Begin,
		End:       member.End,
		Expire:    member.Expire,
		CreatedAt: member.CreatedAt,
		UpdatedAt: member.UpdatedAt,
	}
}

// ----------------------------------------------------------------------

// Retrieves member from middlewares.Authentication().
func AuthenGetMember(c *gin.Context) *dbi.Member {
	m, _ := c.Get("member")
	member := m.(*dbi.Member)
	return member
}
