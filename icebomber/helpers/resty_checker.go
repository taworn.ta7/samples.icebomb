package helpers

import (
	"icebomb/icebomber/logger"

	"github.com/gin-gonic/gin"
	"github.com/go-resty/resty/v2"
)

func RestyCheck(c *gin.Context, resp *resty.Response, err error) bool {
	log := logger.Get().Logger
	id, _ := c.Get("id")

	if v, ok := err.(*resty.ResponseError); ok {
		log.Printf("%v; %v, %v", id, v.Response, v.Err)
		c.Error(ErrSignInExternal)
		return false
	}
	if resp.RawResponse.StatusCode != 200 {
		log.Printf("%v; error: %v", id, resp)
		c.Error(ErrSignInExternal)
		return false
	}

	return true
}
