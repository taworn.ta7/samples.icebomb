package helpers

import (
	"math/rand"
	"time"

	"github.com/oklog/ulid"
)

// Generates ULID.  This function provided as wrapper,
// when you want to change other library.
func GenerateULID() string {
	t := time.Now()
	entropy := ulid.Monotonic(rand.New(rand.NewSource(t.UnixMilli())), 0)
	return ulid.MustNew(uint64(t.UnixMilli()), entropy).String()
}
