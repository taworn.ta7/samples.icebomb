package helpers

import (
	"fmt"
	"net/http"
)

// Simple status error type.
type StatusError struct {
	StatusCode  int
	Description string
}

func (e *StatusError) Error() string {
	if len(e.Description) > 0 {
		return e.Description
	} else {
		return fmt.Sprintf("HTTP status code: %v", e.StatusCode)
	}
}

// ----------------------------------------------------------------------

// Locale-based error type.
type LocaleError struct {
	StatusCode int
	En         string
	Th         string
}

func (e *LocaleError) Error() string {
	return e.En
}

// ----------------------------------------------------------------------

var (
	//
	// 400 Bad Request
	//

	ErrJSONSyntax = &LocaleError{
		StatusCode: http.StatusBadRequest,
		En:         `JSON is malform!`,
		Th:         `JSON ไม่ถูกต้อง!`,
	}

	ErrValidationFailed = &LocaleError{
		StatusCode: http.StatusBadRequest,
		En:         `Validation is failed!`,
		Th:         `การตรวจสอบข้อมูลไม่ถูกต้อง!`,
	}

	ErrUploadIsNotFound = &LocaleError{
		StatusCode: http.StatusBadRequest,
		En:         `Uploaded file is not found!`,
		Th:         `ไม่มีไฟล์ที่ Upload ขึ้นมา!`,
	}

	ErrUploadIsNotType = &LocaleError{
		StatusCode: http.StatusBadRequest,
		En:         `Uploaded file is not a specify type!`,
		Th:         `ไฟล์ที่ Upload ขึ้นมา ไม่ใช่ไฟล์ชนิดที่ต้องการ!`,
	}

	ErrUploadIsNotTypeImage = &LocaleError{
		StatusCode: http.StatusBadRequest,
		En:         `Uploaded file is not an image file!`,
		Th:         `ไฟล์ที่ Upload ขึ้นมา ไม่ใช่ไฟล์รูปภาพ!`,
	}

	ErrUploadIsTooBig = &LocaleError{
		StatusCode: http.StatusBadRequest,
		En:         `Uploaded file size is too big!`,
		Th:         `ไฟล์ที่ Upload ขึ้นมา มีขนาดใหญ่เกินไป!`,
	}

	// ----------------------------------------------------------------------

	//
	// 401 Unauthorized
	//

	ErrTimeout = &LocaleError{
		StatusCode: http.StatusUnauthorized,
		En:         `Timeout, you need to sign-in again!`,
		Th:         `หมดเวลา, คุณต้อง sign-in เข้าระบบอีกครั้ง!`,
	}

	ErrNeedSignIn = &LocaleError{
		StatusCode: http.StatusUnauthorized,
		En:         `You require sign-in!`,
		Th:         `คุณต้อง sign-in เข้าระบบ!`,
	}

	ErrEmailOrPasswordInvalid = &LocaleError{
		StatusCode: http.StatusUnauthorized,
		En:         `Your email or password is incorrect!`,
		Th:         `คุณใส่อีเมลหรือรหัสผ่านไม่ถูกต้อง!`,
	}

	ErrMemberIsResigned = &LocaleError{
		StatusCode: http.StatusUnauthorized,
		En:         `Your membership is resigned!`,
		Th:         `คุณลาออกจากระบบไปแล้ว!`,
	}

	ErrMemberIsDisabledByAdmin = &LocaleError{
		StatusCode: http.StatusUnauthorized,
		En:         `Your membership is disabled!`,
		Th:         `คุณถูกระงับการใช้งาน!`,
	}

	ErrPasswordIsIncorrect = &LocaleError{
		StatusCode: http.StatusUnauthorized,
		En:         `Password is incorrect!`,
		Th:         `รหัสผ่านไม่ถูกต้อง!`,
	}

	ErrMemberRequired = &LocaleError{
		StatusCode: http.StatusUnauthorized,
		En:         `Member rights is required!`,
		Th:         `ต้องการสิทธิ Member!`,
	}

	ErrAdminRequired = &LocaleError{
		StatusCode: http.StatusUnauthorized,
		En:         `Admin rights is required!`,
		Th:         `ต้องการสิทธิ Admin!`,
	}

	ErrSignInExternal = &LocaleError{
		StatusCode: http.StatusUnauthorized,
		En:         `Sign-in external error!`,
		Th:         `การล๊อกอินข้างนอกล้มเหลว!`,
	}

	// ----------------------------------------------------------------------

	//
	// 403 Forbidden
	//

	ErrAlreadyExists = &LocaleError{
		StatusCode: http.StatusForbidden,
		En:         `Data is already exists!`,
		Th:         `มีข้อมูลที่ต้องการอยู่แล้ว!`,
	}

	// ----------------------------------------------------------------------

	//
	// 404 Not Found
	//

	ErrNotFound = &LocaleError{
		StatusCode: http.StatusNotFound,
		En:         `Data is not found!`,
		Th:         `ไม่พบข้อมูลที่ต้องการ!`,
	}
)
