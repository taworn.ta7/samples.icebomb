package helpers

import (
	"icebomb/icebomber/config"

	gomail "gopkg.in/gomail.v2"
)

// Mail sending data type.
type MailSend struct {
	From    string
	To      string
	Subject string
	Body    string
}

// Sends email.
func SendMail(send *MailSend) {
	conf := config.Get()
	message := gomail.NewMessage()
	message.SetHeader("From", send.From)
	message.SetHeader("To", send.To)
	message.SetHeader("Subject", send.Subject)
	message.SetBody("text/html", send.Body)
	mail := gomail.NewDialer(conf.MailHost, conf.MailPort, conf.MailUser, conf.MailPass)
	if err := mail.DialAndSend(message); err != nil {
		panic(err)
	}
}
