package dbi

import (
	"gorm.io/gorm"
)

// Member's credential entity.
type MemberCredential struct {
	gorm.Model `json:"-"`
	ID         string  `json:"id" binding:"required"`
	Salt       string  `json:"salt" binding:"required" gorm:"type:varchar(255);not null"`
	Hash       string  `json:"hash" binding:"required" gorm:"type:varchar(1024);not null"`
	Token      *string `json:"token" binding:"required" gorm:"type:varchar(256);uniqueIndex"`
}
