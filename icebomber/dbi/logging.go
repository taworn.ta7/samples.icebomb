package dbi

import (
	"time"

	"gorm.io/gorm"
)

// Generic logging.
type Logging struct {
	gorm.Model  `json:"-"`
	ID          uint      `json:"id" binding:"required"`
	MemberId    string    `json:"memberId" binding:"required" gorm:"type:varchar(50)"`
	Action      string    `json:"action" binding:"required" gorm:"type:varchar(50)"`
	Table       string    `json:"table" binding:"required" gorm:"type:varchar(100)"`
	Description string    `json:"description" binding:"required"`
	CreatedAt   time.Time `json:"created" gorm:"autoCreateTime"`
	UpdatedAt   time.Time `json:"updated" gorm:"autoUpdateTime"`
}
