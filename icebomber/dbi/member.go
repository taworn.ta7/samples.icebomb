package dbi

import (
	"time"

	"gorm.io/gorm"
)

// Member role constants.
const (
	MemberRoleMember = "member"
	MemberRoleAdmin  = "admin"
)

// Member entity.
type Member struct {
	gorm.Model `json:"-"`
	ID         string           `json:"id" binding:"required"`
	Email      string           `json:"email" binding:"required" gorm:"type:varchar(254);not null;uniqueIndex"`
	Role       string           `json:"role" binding:"required" gorm:"type:varchar(50);not null"`
	Locale     string           `json:"locale" binding:"required" gorm:"type:varchar(10);not null"`
	Name       string           `json:"name" binding:"required" gorm:"type:varchar(200);not null"`
	Disabled   *time.Time       `json:"disabled" binding:"required"`
	Resigned   *time.Time       `json:"resigned" binding:"required"`
	Begin      *time.Time       `json:"begin" binding:"required"`
	End        *time.Time       `json:"end" binding:"required"`
	Expire     *time.Time       `json:"expire" binding:"required"`
	CreatedAt  time.Time        `json:"created" gorm:"autoCreateTime"`
	UpdatedAt  time.Time        `json:"updated" gorm:"autoUpdateTime"`
	Credential MemberCredential `json:"-" gorm:"foreignKey:ID"`
}
