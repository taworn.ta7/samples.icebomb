package dbi

import (
	"time"

	"gorm.io/gorm"
)

// Member password reset entity.
type MemberReset struct {
	gorm.Model   `json:"-"`
	ID           uint      `json:"id" binding:"required"`
	Email        string    `json:"email" binding:"required" gorm:"type:varchar(254);not null"`
	ConfirmToken string    `json:"confirmToken" binding:"required" gorm:"type:varchar(128);not null;uniqueIndex"`
	CreatedAt    time.Time `json:"created" gorm:"autoCreateTime"`
	UpdatedAt    time.Time `json:"updated" gorm:"autoUpdateTime"`
}
