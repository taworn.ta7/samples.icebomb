package dbi

import (
	"gorm.io/gorm"
)

// Member's settings entity.
type MemberSettings struct {
	gorm.Model `json:"-"`
	ID         string `json:"id" binding:"required"`
	MemberID   string `json:"memberId" binding:"required" gorm:"type:varchar(50);not null;index:idx_member"`
	Key        string `json:"key" binding:"required" gorm:"type:varchar(255);not null;index:idx_member"`
	Value      string `json:"value" binding:"required" gorm:"type:varchar(1024);not null"`
}
