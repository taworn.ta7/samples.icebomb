// Package database interface for icebomber.
package dbi

import (
	"fmt"
	"icebomb/icebomber/config"
	"icebomb/icebomber/logger"
	"path/filepath"

	"time"

	l "gorm.io/gorm/logger"

	"github.com/glebarez/sqlite"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// Database interface structure data type.
type DbiType struct {
	Db *gorm.DB
}

var dbi *DbiType

// Retrieves the database interface.
func Get() *DbiType {
	return dbi
}

// ----------------------------------------------------------------------

// Setup the database interface.
func Setup() *DbiType {
	c := config.Get()
	l := l.New(
		logger.Get().Logger,
		l.Config{
			SlowThreshold:             time.Second, // slow SQL threshold
			LogLevel:                  l.Silent,    // log level
			IgnoreRecordNotFoundError: true,        // ignore ErrRecordNotFound error for logger
			Colorful:                  false,       // disable color
		},
	)

	var db *gorm.DB
	var err error
	if c.DbUse == "mysql" {
		dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?charset=utf8mb4&parseTime=True&loc=Local",
			c.DbUser, c.DbPass, c.DbHost, c.DbPort, c.DbName)
		db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{
			Logger: l,
		})
	} else {
		dsn := fmt.Sprintf("%v", filepath.Join(c.StorageDir, c.DbFile))
		db, err = gorm.Open(sqlite.Open(dsn), &gorm.Config{
			Logger: l,
		})
	}
	if err != nil {
		fmt.Printf("GORM is not initialize!\n")
		panic(err)
	}

	db.AutoMigrate(&Logging{})
	db.AutoMigrate(&MemberSignUp{})
	db.AutoMigrate(&MemberReset{})
	db.AutoMigrate(&MemberCredential{})
	db.AutoMigrate(&MemberSettings{})
	db.AutoMigrate(&Member{})

	dbType := &DbiType{
		Db: db,
	}
	dbi = dbType
	return dbType
}
