package dbi

import (
	"time"

	"gorm.io/gorm"
)

// Member sign-up entity.
type MemberSignUp struct {
	gorm.Model   `json:"-"`
	ID           uint      `json:"id" binding:"required"`
	Email        string    `json:"email" binding:"required" gorm:"type:varchar(254);not null"`
	Locale       string    `json:"locale" binding:"required" gorm:"type:varchar(10);not null"`
	Salt         string    `json:"salt" binding:"required" gorm:"type:varchar(255);not null"`
	Hash         string    `json:"hash" binding:"required" gorm:"type:varchar(1024);not null"`
	ConfirmToken string    `json:"confirmToken" binding:"required" gorm:"type:varchar(128);not null;uniqueIndex"`
	CreatedAt    time.Time `json:"created" gorm:"autoCreateTime"`
	UpdatedAt    time.Time `json:"updated" gorm:"autoUpdateTime"`
}
