package middlewares

import (
	"errors"
	"icebomb/icebomber/config"
	"icebomb/icebomber/dbi"
	"icebomb/icebomber/helpers"
	"icebomb/icebomber/logger"
	"strings"
	"time"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

const (
	CheckAuthenOptional = 0 // checks is an optional
	CheckAuthenMember   = 1 // checks must pass member role
	CheckAuthenAdmin    = 2 // checks must pass admin role
	CheckAuthen         = 3 // checks must pass any role
)

// Checks authentication before go on.
func Authentication(check int) gin.HandlerFunc {
	return func(c *gin.Context) {
		//log := logger.Get().Logger

		member, err := MemberFromHeaders(c)
		c.Set("member", member)

		if check > 0 {
			if check == CheckAuthenMember {
				if !(member != nil && member.Role == dbi.MemberRoleMember) {
					c.Error(helpers.ErrMemberRequired)
					c.Abort()
					return
				}
			} else if check == CheckAuthenAdmin {
				if !(member != nil && member.Role == dbi.MemberRoleAdmin) {
					c.Error(helpers.ErrAdminRequired)
					c.Abort()
					return
				}
			}
			if member == nil {
				c.Error(err)
				c.Abort()
				return
			}
		}

		c.Next()
	}
}

// ----------------------------------------------------------------------

func TokenFromHeaders(c *gin.Context) string {
	s := c.Request.Header.Get("authorization")
	if len(s) > 0 {
		a := strings.Split(s, " ")
		if len(a) >= 2 {
			if strings.ToLower(a[0]) == "bearer" {
				return a[1]
			}
		}
	}
	return ""
}

func MemberFromHeaders(c *gin.Context) (*dbi.Member, error) {
	log := logger.Get().Logger
	db := dbi.Get().Db
	id, _ := c.Get("id")

	// token from headers
	token := TokenFromHeaders(c)
	if len(token) <= 0 {
		return nil, helpers.ErrNeedSignIn
	}

	// checks if this token is a valid member
	var member *dbi.Member
	{
		result := db.Joins("Credential").First(&member, "Credential.token = ?", token)
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, helpers.ErrNeedSignIn
		}

	}
	log.Printf("%v; %v/%v, role=%v, name=%v", id, member.ID, member.Email, member.Role, member.Name)

	// checks if this member session is expired or not
	now := time.Now()
	if member.Expire == nil || member.Expire.Before(now) {
		return nil, helpers.ErrNeedSignIn
	}

	// updates sign-in timeout
	expire := now.Add(time.Duration(config.Get().AuthenTimeout * int64(time.Millisecond)))
	member.Expire = &expire
	db.Model(&member).Updates(&dbi.Member{
		Expire: member.Expire,
	})
	return member, nil
}
