package middlewares

import (
	"icebomb/icebomber/logger"

	"github.com/gin-gonic/gin"
)

// Dumps HTTP headers and body middleware.
func Dump(dumpHeader bool, dumpBody bool) gin.HandlerFunc {
	return func(c *gin.Context) {
		log := logger.Get().Logger
		id, _ := c.Get("id")

		if dumpHeader {
			log.Printf("%v; Header: %v", id, logger.Dump(c.Request.Header))
		}

		if dumpBody {
			log.Printf("%v; Body: %v", id, logger.Dump(c.Request.Body))
		}

		c.Next()
	}
}
