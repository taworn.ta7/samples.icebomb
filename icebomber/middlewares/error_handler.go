package middlewares

import (
	"errors"
	"icebomb/icebomber/helpers"
	"icebomb/icebomber/logger"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// Error handling middleware.
func ErrorHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()

		if len(c.Errors) > 0 {
			log := logger.Get().Logger
			id, _ := c.Get("id")

			e := c.Errors[0]

			{
				var err *helpers.LocaleError
				if errors.As(e, &err) {
					json := gin.H{
						"statusCode": err.StatusCode,
						"locales": gin.H{
							"en": err.En,
							"th": err.Th,
						},
						"path":      c.Request.URL.Path,
						"requestId": id,
						"timestamp": time.Now(),
					}
					c.AbortWithStatusJSON(err.StatusCode, json)
					log.Printf("%v; %v %v: %v %v; [ERROR] %v",
						id, c.Request.Method, c.Request.RequestURI, err.StatusCode, http.StatusText(err.StatusCode), err.En)
					return
				}
			}

			{
				var err *helpers.StatusError
				if errors.As(e, &err) {
					json := gin.H{
						"statusCode":  err.StatusCode,
						"description": err.Description,
						"path":        c.Request.URL.Path,
						"requestId":   id,
						"timestamp":   time.Now(),
					}
					c.AbortWithStatusJSON(err.StatusCode, json)
					log.Printf("%v; %v %v: %v %v; [ERROR] %v",
						id, c.Request.Method, c.Request.RequestURI, err.StatusCode, http.StatusText(err.StatusCode), err.Description)
					return
				}
			}

			json := gin.H{
				"statusCode": http.StatusInternalServerError,
				"path":       c.Request.URL.Path,
				"requestId":  id,
				"timestamp":  time.Now(),
			}
			c.AbortWithStatusJSON(http.StatusInternalServerError, json)
			log.Printf("%v; %v %v: %v %v; [ERROR] %v",
				id, c.Request.Method, c.Request.RequestURI, http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), logger.Dump(e))
		}
	}
}
