package middlewares

import (
	"icebomb/icebomber/helpers"
	"icebomb/icebomber/logger"

	"github.com/gin-gonic/gin"
)

// Logging middleware.
func Logging() gin.HandlerFunc {
	return func(c *gin.Context) {
		log := logger.Get().Logger

		id := helpers.GenerateULID()
		c.Set("id", id)
		log.Printf("%v; %v %v...", id, c.Request.Method, c.Request.RequestURI)

		c.Next()

		if !c.IsAborted() {
			log.Printf("%v; %v %v: DONE", id, c.Request.Method, c.Request.RequestURI)
		}
	}
}
