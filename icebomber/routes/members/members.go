package members

import (
	"errors"
	"icebomb/icebomber/config"
	"icebomb/icebomber/dbi"
	"icebomb/icebomber/helpers"
	"icebomb/icebomber/logger"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// Lists members with conditions.  All parameters are optional.
func List(c *gin.Context) {
	//conf := config.Get()
	log := logger.Get().Logger
	db := dbi.Get().Db
	id, _ := c.Get("id")

	sortDict := &map[string]string{
		"email":   "email",
		"name":    "name",
		"created": "created_at",
		"updated": "updated_at",
	}

	// binds query to variable(s)
	rowsPerPage := helpers.AtoiWithDefault(c.Query("size"), 10)
	page := helpers.AtoiWithDefault(c.Query("page"), 0)
	order := helpers.GetOrderingAsString(sortDict, c.DefaultQuery("order", ""))
	search := c.DefaultQuery("search", "")
	trash := helpers.AtoiWithDefault(c.Query("trash"), 0) != 0
	log.Printf("%v; rowsPerPage: %v, page: %v, order: %v, search: %v, trash: %v", id, rowsPerPage, page, order, search, trash)

	var params []interface{}
	params = append(params, "admin")
	query := "role <> ?"

	if !trash {
		query += " AND (disabled IS NULL AND resigned IS NULL)"
	} else {
		query += " AND NOT (disabled IS NULL AND resigned IS NULL)"
	}

	if len(search) > 0 {
		params = append(params, "%"+search+"%", "%"+search+"%")
		query += " AND (email LIKE ? OR name LIKE ?)"
	}

	log.Printf("%v; query: %v", id, query)
	log.Printf("%v; params: %v", id, logger.Dump(params))

	var members []dbi.Member
	db.
		Limit(rowsPerPage).
		Offset(page*rowsPerPage).
		Order(order).
		Where(query, params...).
		Find(&members)

	var count int64
	db.Model(&dbi.Member{}).
		//Order(order).
		Where(query, params...).
		Count(&count)
	log.Printf("%v; count: %v", id, count)

	c.JSON(http.StatusOK, gin.H{
		"members":    members,
		"pagination": helpers.GetPagination(page, rowsPerPage, count),
	})
}

// ----------------------------------------------------------------------

// Gets member's information.
func Get(c *gin.Context) {
	//conf := config.Get()
	//log := logger.Get().Logger
	db := dbi.Get().Db
	//id, _ := c.Get("id")

	// gets member
	email := c.Param("email")
	var member *dbi.Member
	{
		result := db.First(&member, "email = ?", email)
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			c.Error(helpers.ErrNotFound)
			return
		}
	}

	c.JSON(http.StatusOK, gin.H{
		"member": member,
	})
}

// ----------------------------------------------------------------------

// Gets member's icons.
func GetIcon(c *gin.Context) {
	conf := config.Get()
	log := logger.Get().Logger
	db := dbi.Get().Db
	id, _ := c.Get("id")

	// gets member
	email := c.Param("email")
	var member *dbi.Member
	{
		result := db.First(&member, "email = ?", email)
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			c.Error(helpers.ErrNotFound)
			return
		}
	}

	// gets folder
	cwd, _ := os.Getwd()
	dir := filepath.Join(cwd, conf.UploadDir, member.ID, "profile")
	log.Printf("%v; directory: %v", id, dir)

	// gets icon or default icon
	var mime string
	var icon string
	stat, err := os.Stat(filepath.Join(dir, "icon"))
	if !errors.Is(err, os.ErrNotExist) {
		m, _ := os.ReadFile(filepath.Join(dir, "icon-mime"))
		mime = string(m)
		icon = filepath.Join(dir, "icon")
	} else {
		mime = "image/png"
		icon = filepath.Join(cwd, "assets", "default-profile-icon.png")
		stat, _ = os.Stat(icon)
	}

	// opens file
	f, _ := os.Open(icon)
	defer f.Close()

	c.DataFromReader(http.StatusOK, stat.Size(), string(mime), f, map[string]string{})
}
