package members

import (
	"errors"
	"fmt"
	"icebomb/icebomber/config"
	"icebomb/icebomber/dbi"
	"icebomb/icebomber/helpers"
	"icebomb/icebomber/logger"
	"icebomb/icebomber/mail_templates"
	"net/http"
	"strings"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Sign-up DTO wrapper type.
type SignUpWrapType struct {
	Member SignUpType `json:"member"`
}

// Sign-up DTO type.
type SignUpType struct {
	Email           string `json:"email" binding:"required" validate:"required,email"`
	Password        string `json:"password" binding:"required" validate:"required,min=4,max=20"`
	ConfirmPassword string `json:"confirmPassword" binding:"required" validate:"required,min=4,max=20,eqfield=Password"`
	Locale          string `json:"locale" binding:"required" validate:"required,oneof=en th"`
}

// Receives sign-up form and saves it to database.  Returns URL to let user confirm it.
func SignUp(c *gin.Context) {
	conf := config.Get()
	log := logger.Get().Logger
	db := dbi.Get().Db
	id, _ := c.Get("id")

	// binds body to JSON
	var json SignUpWrapType
	if err := c.ShouldBindJSON(&json); err != nil {
		c.Error(helpers.ErrJSONSyntax)
		return
	}
	//log.Printf("%v; JSON: %v", id, logger.Dump(json))
	if !helpers.Validate(c, json) {
		return
	}

	// first, checks this input email
	// this email must not be in member table
	{
		var member *dbi.Member
		result := db.First(&member, "email = ?", json.Member.Email)
		if !errors.Is(result.Error, gorm.ErrRecordNotFound) {
			c.Error(helpers.ErrAlreadyExists)
			return
		}
	}

	// creates member sign-up
	hash, salt := helpers.SetPassword(json.Member.Password)
	signup := dbi.MemberSignUp{
		Email:        json.Member.Email,
		Locale:       json.Member.Locale,
		Hash:         hash,
		Salt:         salt,
		ConfirmToken: helpers.GenerateSecret(64),
	}
	db.Create(&signup)

	// creates url
	url := helpers.Protocol(c) + c.Request.Host + c.Request.URL.Path + "/confirm?code=" + signup.ConfirmToken
	log.Printf("%v; current URL: %v", id, url)

	// sends email
	log.Printf("%v; Sign-up: %v", id, logger.Dump(signup))
	helpers.SendMail(&helpers.MailSend{
		From:    conf.MailAdmin,
		To:      signup.Email,
		Subject: "Sign-up confirmation email",
		Body: mail_templates.SignUp(&mail_templates.SignUpTemplate{
			Email:        signup.Email,
			URL:          url,
			ConfirmToken: signup.ConfirmToken,
		}),
	})

	c.JSON(http.StatusOK, gin.H{
		"member": signup,
		"url":    url,
	})
}

// ----------------------------------------------------------------------

// Confirms the sign-up form, then, copies sign-up data into member table.
func ConfirmSignUp(c *gin.Context) {
	//conf := config.Get()
	log := logger.Get().Logger
	db := dbi.Get().Db
	id, _ := c.Get("id")

	// binds query to variable(s)
	code := c.DefaultQuery("code", "")

	// checks code
	if strings.TrimSpace(code) == "" {
		c.Error(helpers.ErrNotFound)
		return
	}
	var signUp *dbi.MemberSignUp
	{
		result := db.First(&signUp, "confirm_token = ?", code)
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			c.Error(helpers.ErrNotFound)
			return
		}
	}

	// checks if sign-up data already created member or not
	{
		var check *dbi.Member
		result := db.First(&check, "email = ?", signUp.Email)
		if !errors.Is(result.Error, gorm.ErrRecordNotFound) {
			c.Error(helpers.ErrAlreadyExists)
			return
		}
	}

	// generates name from email
	names := strings.Split(signUp.Email, "@")
	name := names[0]

	// create member
	uid := helpers.GenerateULID()
	member := dbi.Member{
		Model:    gorm.Model{},
		ID:       uid,
		Email:    signUp.Email,
		Role:     dbi.MemberRoleMember,
		Locale:   signUp.Locale,
		Name:     name,
		Disabled: nil,
		Resigned: nil,
		Begin:    nil,
		End:      nil,
		Expire:   nil,
		Credential: dbi.MemberCredential{
			ID:    uid,
			Salt:  signUp.Salt,
			Hash:  signUp.Hash,
			Token: nil,
		},
	}

	// saves
	db.Create(&member)

	// adds log
	db.Create(&dbi.Logging{
		MemberId:    member.ID,
		Action:      "update",
		Table:       "member",
		Description: fmt.Sprintf("Member %v/%v is sign-up and confirm.", member.ID, member.Email),
	})

	log.Printf("%v; Member created: %v", id, logger.Dump(member))
	c.JSON(http.StatusCreated, gin.H{
		"member": member,
	})
}
