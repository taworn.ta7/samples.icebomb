package members

import (
	"errors"
	"fmt"
	"icebomb/icebomber/config"
	"icebomb/icebomber/dbi"
	"icebomb/icebomber/helpers"
	"icebomb/icebomber/logger"
	"icebomb/icebomber/mail_templates"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// Password reset DTO wrapper type.
type ResetWrapType struct {
	Member ResetType `json:"member"`
}

// Password reset DTO type.
type ResetType struct {
	Email string `json:"email" binding:"required" validate:"required,email"`
}

// Receives password reset form and sends email to member.
func RequestReset(c *gin.Context) {
	conf := config.Get()
	log := logger.Get().Logger
	db := dbi.Get().Db
	id, _ := c.Get("id")

	// binds body to JSON
	var json ResetWrapType
	if err := c.ShouldBindJSON(&json); err != nil {
		c.Error(helpers.ErrJSONSyntax)
		return
	}
	//log.Printf("%v; JSON: %v", id, logger.Dump(json))
	if !helpers.Validate(c, json) {
		return
	}

	// first, checks this input email
	// this email must be valid in member table
	{
		var member *dbi.Member
		result := db.First(&member, "email = ?", json.Member.Email)
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			c.Error(helpers.ErrNotFound)
			return
		}
	}

	// creates reset
	reset := dbi.MemberReset{
		Email:        json.Member.Email,
		ConfirmToken: helpers.GenerateSecret(64),
	}
	db.Create(&reset)

	// creates url
	url := helpers.Protocol(c) + c.Request.Host + c.Request.URL.Path + "/reset-password?code=" + reset.ConfirmToken
	log.Printf("%v; current URL: %v", id, url)

	// sends email
	helpers.SendMail(&helpers.MailSend{
		From:    conf.MailAdmin,
		To:      reset.Email,
		Subject: "Reset password email",
		Body: mail_templates.Reset(&mail_templates.ResetTemplate{
			Email:        reset.Email,
			URL:          url,
			ConfirmToken: reset.ConfirmToken,
		}),
	})

	c.JSON(http.StatusOK, gin.H{
		"member": reset,
		"url":    url,
	})
}

// ----------------------------------------------------------------------

// Confirms the password reset.  This will generate new password and send new password to email.
func ResetPassword(c *gin.Context) {
	conf := config.Get()
	log := logger.Get().Logger
	db := dbi.Get().Db
	id, _ := c.Get("id")

	// binds query to variable(s)
	code := c.DefaultQuery("code", "")

	// checks code
	if strings.TrimSpace(code) == "" {
		c.Error(helpers.ErrNotFound)
		return
	}
	var reset *dbi.MemberReset
	{
		result := db.First(&reset, "confirm_token = ?", code)
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			c.Error(helpers.ErrNotFound)
			return
		}
	}

	// checks if reset data must be valid
	var member *dbi.Member
	{
		result := db.Joins("Credential").First(&member, "email = ?", reset.Email)
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			c.Error(helpers.ErrNotFound)
			return
		}
	}

	// generates new password
	generate := helpers.GenerateSecret(8)
	hash, salt := helpers.SetPassword(generate)
	member.Credential.Salt = salt
	member.Credential.Hash = hash
	member.Credential.Token = nil

	// saves
	db.Model(&member.Credential).Updates(&dbi.MemberCredential{
		Salt:  member.Credential.Salt,
		Hash:  member.Credential.Hash,
		Token: member.Credential.Token,
	})

	// adds log
	db.Create(&dbi.Logging{
		MemberId:    member.ID,
		Action:      "update",
		Table:       "member",
		Description: fmt.Sprintf("Member %v/%v is reset password.", member.ID, member.Email),
	})

	// sends email
	helpers.SendMail(&helpers.MailSend{
		From:    conf.MailAdmin,
		To:      reset.Email,
		Subject: "You have reset your password!",
		Body: mail_templates.ResetConfirmed(&mail_templates.ResetConfirmedTemplate{
			Email:    reset.Email,
			Password: generate,
		}),
	})

	log.Printf("%v; Member password reset: %v", id, logger.Dump(member))
	c.JSON(http.StatusOK, gin.H{
		"member": member,
	})
}
