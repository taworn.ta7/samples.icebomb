package settings

import (
	"fmt"
	"icebomb/icebomber/dbi"
	"icebomb/icebomber/helpers"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Settings DTO wrapper type.
type SettingsWrapType struct {
	Member SettingsType `json:"member"`
}

// Settings DTO type.
type SettingsType struct {
	Locale string `json:"locale" binding:"required" validate:"required,oneof=en th"`
}

// Changes current settings.  Currently, it has just one setting: locale.
func Change(c *gin.Context) {
	//conf := config.Get()
	//log := logger.Get().Logger
	db := dbi.Get().Db
	//id, _ := c.Get("id")
	member := helpers.AuthenGetMember(c)

	// binds body to JSON
	var json SettingsWrapType
	if err := c.ShouldBindJSON(&json); err != nil {
		c.Error(helpers.ErrJSONSyntax)
		return
	}
	//log.Printf("%v; JSON: %v", id, logger.Dump(json))
	if !helpers.Validate(c, json) {
		return
	}

	// saves
	member.Locale = json.Member.Locale
	db.Model(&member).Updates(&dbi.Member{
		Locale: member.Locale,
	})

	// adds log
	db.Create(&dbi.Logging{
		MemberId:    member.ID,
		Action:      "update",
		Table:       "member",
		Description: fmt.Sprintf("Member %v/%v settings changed.", member.ID, member.Email),
	})

	c.JSON(http.StatusOK, gin.H{
		"member": member,
	})
}
