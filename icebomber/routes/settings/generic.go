package settings

import (
	"errors"
	"fmt"
	"icebomb/icebomber/config"
	"icebomb/icebomber/dbi"
	"icebomb/icebomber/helpers"
	"net/http"
	"sort"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Sets key-value generic settings.
func Set(c *gin.Context) {
	conf := config.Get()
	//log := logger.Get().Logger
	db := dbi.Get().Db
	//id, _ := c.Get("id")
	member := helpers.AuthenGetMember(c)

	key := c.Param("key")
	value := c.Param("value")

	// checks with available settings list
	// Beware! array availableSettings must be sorted!
	availableSettings := conf.AvailableSettings
	index := sort.SearchStrings(availableSettings, key)
	if !(index < len(availableSettings) && availableSettings[index] == key) {
		c.Error(helpers.ErrNotFound)
		return
	}

	// upserts
	var setting *dbi.MemberSettings
	result := db.First(&setting, "member_id = ? AND `key` = ?", member.ID, key)
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		// inserts
		setting = &dbi.MemberSettings{
			Model:    gorm.Model{},
			ID:       helpers.GenerateULID(),
			MemberID: member.ID,
			Key:      key,
			Value:    value,
		}
		db.Create(&setting)
	} else {
		// updates
		setting.Value = value
		db.Model(&setting).Update("value", value)
	}

	// adds log
	db.Create(&dbi.Logging{
		MemberId:    member.ID,
		Action:      "update",
		Table:       "member",
		Description: fmt.Sprintf("Member %v/%v is set %v=%v.", member.ID, member.Email, key, value),
	})

	c.String(http.StatusOK, "")
}

// Gets key-value generic settings.  Returns value string, or null if key not exists.
func Get(c *gin.Context) {
	//conf := config.Get()
	//log := logger.Get().Logger
	db := dbi.Get().Db
	//id, _ := c.Get("id")
	member := helpers.AuthenGetMember(c)

	key := c.Param("key")
	var value *string
	var setting *dbi.MemberSettings
	result := db.First(&setting, "member_id = ? AND `key` = ?", member.ID, key)
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		value = nil
	} else {
		value = &setting.Value
	}

	c.JSON(http.StatusOK, gin.H{
		"value": value,
	})
}

// Gets all generic settings.  Returns all key-value pairs.
func GetAll(c *gin.Context) {
	//conf := config.Get()
	//log := logger.Get().Logger
	db := dbi.Get().Db
	//id, _ := c.Get("id")
	member := helpers.AuthenGetMember(c)

	var settings []dbi.MemberSettings
	db.Find(&settings, "member_id = ?", member.ID)
	//log.Printf("%v; Settings: %v", id, logger.Dump(settings))

	result := make(map[string]string, len(settings))
	for _, row := range settings {
		result[row.Key] = row.Value
	}

	c.JSON(http.StatusOK, gin.H{
		"settings": result,
	})
}

// Deletes all generic settings.  This function is use for reset.
func Reset(c *gin.Context) {
	//conf := config.Get()
	//log := logger.Get().Logger
	db := dbi.Get().Db
	//id, _ := c.Get("id")
	member := helpers.AuthenGetMember(c)

	var settings []dbi.MemberSettings
	db.Where("member_id = ?", member.ID).Delete(&settings)

	// adds log
	db.Create(&dbi.Logging{
		MemberId:    member.ID,
		Action:      "update",
		Table:       "member",
		Description: fmt.Sprintf("Member %v/%v is reset settings.", member.ID, member.Email),
	})

	c.String(http.StatusOK, "")
}
