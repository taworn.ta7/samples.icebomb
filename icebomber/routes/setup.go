package routes

import (
	"errors"
	"icebomb/icebomber/config"
	"icebomb/icebomber/dbi"
	"icebomb/icebomber/helpers"
	"icebomb/icebomber/logger"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// Setup and creates admin member.  Can run only once.
func Setup(c *gin.Context) {
	conf := config.Get()
	log := logger.Get().Logger
	db := dbi.Get().Db
	id, _ := c.Get("id")

	// checks if we already have 'admin' member?
	{
		var check *dbi.Member
		result := db.First(&check, "email = ? AND role = ?", conf.MailAdmin, dbi.MemberRoleAdmin)
		if !errors.Is(result.Error, gorm.ErrRecordNotFound) {
			c.String(http.StatusOK, `Already setup.`)
			return
		}
	}

	// creates member 'admin'
	pass, salt := helpers.SetPassword("admin")
	uid := helpers.GenerateULID()
	member := dbi.Member{
		Model:    gorm.Model{},
		ID:       uid,
		Email:    conf.MailAdmin,
		Role:     dbi.MemberRoleAdmin,
		Locale:   "en",
		Name:     "Administrator",
		Disabled: nil,
		Resigned: nil,
		Begin:    nil,
		End:      nil,
		Expire:   nil,
		Credential: dbi.MemberCredential{
			ID:    uid,
			Salt:  salt,
			Hash:  pass,
			Token: nil,
		},
	}

	// saves
	db.Create(&member)

	log.Printf("%v; Admin created: %v", id, logger.Dump(member))
	c.String(http.StatusCreated, `Setup completed :)`)
}
