package profile

import (
	"fmt"
	"icebomb/icebomber/dbi"
	"icebomb/icebomber/helpers"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Changes name DTO wrapper type.
type ChangeNameWrapType struct {
	Member ChangeNameType `json:"member"`
}

// Changes name DTO type.
type ChangeNameType struct {
	Name string `json:"name" binding:"required" validate:"required,min=1,max=200"`
}

// Changes the current name.
func ChangeName(c *gin.Context) {
	//conf := config.Get()
	//log := logger.Get().Logger
	db := dbi.Get().Db
	//id, _ := c.Get("id")
	member := helpers.AuthenGetMember(c)

	// binds body to JSON
	var json ChangeNameWrapType
	if err := c.ShouldBindJSON(&json); err != nil {
		c.Error(helpers.ErrJSONSyntax)
		return
	}
	//log.Printf("%v; JSON: %v", id, logger.Dump(json))
	if !helpers.Validate(c, json) {
		return
	}

	// saves
	member.Name = json.Member.Name
	db.Model(&member).Updates(&dbi.Member{
		Name: member.Name,
	})

	// adds log
	db.Create(&dbi.Logging{
		MemberId:    member.ID,
		Action:      "update",
		Table:       "member",
		Description: fmt.Sprintf("Member %v/%v changed name.", member.ID, member.Email),
	})

	c.JSON(http.StatusOK, gin.H{
		"member": member,
	})
}
