package profile

import (
	"errors"
	"fmt"
	"icebomb/icebomber/config"
	"icebomb/icebomber/dbi"
	"icebomb/icebomber/helpers"
	"icebomb/icebomber/logger"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gin-gonic/gin"
)

// Uploads picture to use as member profile icon.
func Upload(c *gin.Context) {
	conf := config.Get()
	log := logger.Get().Logger
	db := dbi.Get().Db
	id, _ := c.Get("id")
	member := helpers.AuthenGetMember(c)

	// checks uploaded file
	file, err := c.FormFile("image")
	if err != nil {
		c.Error(helpers.ErrUploadIsNotFound)
		return
	}
	//log.Printf("%v; file: %v", id, logger.Dump(file))

	// checks uploaded file is image type
	mime := file.Header.Get("Content-Type")
	if mime != "image/png" && mime != "image/jpeg" && mime != "image/gif" {
		c.Error(helpers.ErrUploadIsNotTypeImage)
		return
	}

	// checks uploaded file is not too big
	limit := conf.ProfileIconFileLimit
	if file.Size >= limit {
		c.Error(helpers.ErrUploadIsTooBig)
		return
	}

	// creates folder, if not exists
	cwd, _ := os.Getwd()
	dir := filepath.Join(cwd, conf.UploadDir, member.ID, "profile")
	os.MkdirAll(dir, os.ModePerm)
	log.Printf("%v; directory: %v", id, dir)

	// moves uploaded file
	c.SaveUploadedFile(file, filepath.Join(dir, "icon"))

	// saves mime
	os.WriteFile(filepath.Join(dir, "icon-mime"), []byte(mime+"\n"), 0666)

	// adds log
	db.Create(&dbi.Logging{
		MemberId:    member.ID,
		Action:      "update",
		Table:       "member",
		Description: fmt.Sprintf("Member %v/%v uploaded profile icon.", member.ID, member.Email),
	})

	// generates output
	name := "icon"
	part := fmt.Sprintf("/%v/%v/profile", conf.UploadDir, member.ID)
	host := c.Request.Host
	protocol := helpers.Protocol(c)

	c.JSON(http.StatusOK, gin.H{
		"image": gin.H{
			"protocol": protocol,
			"host":     host,
			"path":     part,
			"name":     name,
			"url":      fmt.Sprintf("%v%v%v/%v", protocol, host, part, name),
		},
	})
}

// ----------------------------------------------------------------------

// Views the uploaded profile icon.
func View(c *gin.Context) {
	conf := config.Get()
	log := logger.Get().Logger
	//db := dbi.Get().Db
	id, _ := c.Get("id")
	member := helpers.AuthenGetMember(c)

	// gets folder
	cwd, _ := os.Getwd()
	dir := filepath.Join(cwd, conf.UploadDir, member.ID, "profile")
	log.Printf("%v; directory: %v", id, dir)

	// gets icon or default icon
	var mime string
	var icon string
	stat, err := os.Stat(filepath.Join(dir, "icon"))
	if !errors.Is(err, os.ErrNotExist) {
		m, _ := os.ReadFile(filepath.Join(dir, "icon-mime"))
		mime = string(m)
		icon = filepath.Join(dir, "icon")
	} else {
		mime = "image/png"
		icon = filepath.Join(cwd, "assets", "default-profile-icon.png")
		stat, _ = os.Stat(icon)
	}

	// opens file
	f, _ := os.Open(icon)
	defer f.Close()

	c.DataFromReader(http.StatusOK, stat.Size(), string(mime), f, map[string]string{})
}

// ----------------------------------------------------------------------

// Deletes the uploaded profile icon and reverts profile icon to defaults.
func Delete(c *gin.Context) {
	conf := config.Get()
	log := logger.Get().Logger
	db := dbi.Get().Db
	id, _ := c.Get("id")
	member := helpers.AuthenGetMember(c)

	// gets folder
	cwd, _ := os.Getwd()
	dir := filepath.Join(cwd, conf.UploadDir, member.ID, "profile")
	log.Printf("%v; directory: %v", id, dir)

	// removes icon and mime
	os.Remove(filepath.Join(dir, "icon"))
	os.Remove(filepath.Join(dir, "icon-mime"))

	// adds log
	db.Create(&dbi.Logging{
		MemberId:    member.ID,
		Action:      "update",
		Table:       "member",
		Description: fmt.Sprintf("Member %v/%v deleted profile icon.", member.ID, member.Email),
	})

	c.JSON(http.StatusOK, gin.H{
		"member": member,
	})
}
