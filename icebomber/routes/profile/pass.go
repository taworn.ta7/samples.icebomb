package profile

import (
	"fmt"
	"icebomb/icebomber/dbi"
	"icebomb/icebomber/helpers"
	"icebomb/icebomber/logger"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// Changes password DTO wrapper type.
type ChangePasswordWrapType struct {
	Member ChangePasswordType `json:"member"`
}

// Changes password DTO type.
type ChangePasswordType struct {
	CurrentPassword string `json:"currentPassword" binding:"required" validate:"required,min=4,max=20"`
	NewPassword     string `json:"newPassword" binding:"required" validate:"required,min=4,max=20"`
	ConfirmPassword string `json:"confirmPassword" binding:"required" validate:"required,min=4,max=20,eqfield=NewPassword"`
}

// Changes the current password.  After the password is changed, you will be sign-out and need to sign-in again.
func ChangePassword(c *gin.Context) {
	//conf := config.Get()
	log := logger.Get().Logger
	db := dbi.Get().Db
	id, _ := c.Get("id")
	member := helpers.AuthenGetMember(c)

	// binds body to JSON
	var json ChangePasswordWrapType
	if err := c.ShouldBindJSON(&json); err != nil {
		c.Error(helpers.ErrJSONSyntax)
		return
	}
	log.Printf("%v; JSON: %v", id, logger.Dump(json))
	if !helpers.Validate(c, json) {
		return
	}

	// validates current password
	if !helpers.ValidatePassword(json.Member.CurrentPassword, member.Credential.Hash) {
		c.Error(helpers.ErrPasswordIsIncorrect)
		return
	}

	// generates new password
	generate := json.Member.NewPassword
	hash, salt := helpers.SetPassword(generate)
	member.Credential.Salt = salt
	member.Credential.Hash = hash
	member.Credential.Token = nil

	// saves
	now := time.Now()
	member.End = &now
	db.Model(&member).Updates(&dbi.Member{
		End: member.End,
	})
	db.Model(&member.Credential).Updates(&dbi.MemberCredential{
		Salt:  member.Credential.Salt,
		Hash:  member.Credential.Hash,
		Token: member.Credential.Token,
	})
	// BUG: don't know why, but this bug Token = nil but it not work!
	// SOLUTION: when you update NULL, have to do Update() NOT Updates()
	db.Model(&member.Credential).Update("token", member.Credential.Token)

	// adds log
	db.Create(&dbi.Logging{
		MemberId:    member.ID,
		Action:      "update",
		Table:       "member",
		Description: fmt.Sprintf("Member %v/%v changed password!", member.ID, member.Email),
	})

	log.Printf("%v; Member password changed: %v", id, logger.Dump(member))
	c.JSON(http.StatusOK, gin.H{
		"member": member,
	})
}
