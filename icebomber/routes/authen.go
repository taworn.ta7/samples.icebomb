package routes

import (
	"errors"
	"fmt"
	"icebomb/icebomber/dbi"
	"icebomb/icebomber/helpers"
	"icebomb/icebomber/logger"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

// Sign-in DTO wrapper type.
type SignInWrapType struct {
	SignIn SignInType `json:"signin"`
}

// Sign-in DTO type.
type SignInType struct {
	Email    string `json:"email" binding:"required" validate:"required,email"`
	Password string `json:"password" binding:"required" validate:"required,min=4,max=20"`
}

// Authorizes the email and password.  Returns member data and sign-in token and will be use all the session.
func SignIn(c *gin.Context) {
	//conf := config.Get()
	log := logger.Get().Logger
	db := dbi.Get().Db
	id, _ := c.Get("id")

	// binds body to JSON
	var json SignInWrapType
	if err := c.ShouldBindJSON(&json); err != nil {
		c.Error(helpers.ErrJSONSyntax)
		return
	}
	log.Printf("%v; JSON: %v", id, logger.Dump(json))
	if !helpers.Validate(c, json) {
		return
	}

	// selects member
	var member *dbi.Member
	{
		result := db.Joins("Credential").First(&member, "email = ?", json.SignIn.Email)
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			c.Error(helpers.ErrNotFound)
			return
		}
	}
	if !helpers.ValidatePassword(json.SignIn.Password, member.Credential.Hash) {
		c.Error(helpers.ErrEmailOrPasswordInvalid)
		return
	}

	// check if this member resigned or disabled
	if member.Resigned != nil {
		c.Error(helpers.ErrMemberIsResigned)
		return
	}
	if member.Disabled != nil {
		c.Error(helpers.ErrMemberIsDisabledByAdmin)
		return
	}

	// checks if expiry sign-in
	t := helpers.NewOrUpdateToken(member)
	log.Printf("%v; token: %v", id, t)

	// updates member
	db.Model(&member).Updates(&dbi.Member{
		Begin:  member.Begin,
		End:    member.End,
		Expire: member.Expire,
	})
	db.Model(&member.Credential).Update("token", member.Credential.Token)

	// adds log
	db.Create(&dbi.Logging{
		MemberId:    member.ID,
		Action:      "update",
		Table:       "member",
		Description: fmt.Sprintf("Member %v/%v is sign-in.", member.ID, member.Email),
	})

	log.Printf("%v; Member sign-in: %v/%v, role=%v", id, member.ID, member.Email, member.Role)
	c.JSON(http.StatusOK, gin.H{
		"member": member,
		"token":  member.Credential.Token,
	})
}

// ----------------------------------------------------------------------

// Signs off from current session.  The sign-in token will be invalid.
func SignOut(c *gin.Context) {
	//config := config.Get()
	log := logger.Get().Logger
	db := dbi.Get().Db
	id, _ := c.Get("id")
	member := helpers.AuthenGetMember(c)

	// updates member
	now := time.Now()
	member.End = &now
	member.Credential.Token = nil
	db.Model(&member).Updates(&dbi.Member{
		End: member.End,
	})
	db.Model(&member.Credential).Update("token", member.Credential.Token)

	// adds log
	db.Create(&dbi.Logging{
		MemberId:    member.ID,
		Action:      "update",
		Table:       "member",
		Description: fmt.Sprintf("Member %v/%v is sign-out.", member.ID, member.Email),
	})

	log.Printf("%v; Member sign-out: %v/%v, role=%v", id, member.ID, member.Email, member.Role)
	c.JSON(http.StatusOK, gin.H{
		"member": member,
	})
}

// ----------------------------------------------------------------------

// Checks current sign-in state.
func Check(c *gin.Context) {
	//config := config.Get()
	//log := logger.Get().Logger
	//db := dbi.Get().Db
	//id, _ := c.Get("id")
	member := helpers.AuthenGetMember(c)

	c.JSON(http.StatusOK, gin.H{
		"member": member,
	})
}
