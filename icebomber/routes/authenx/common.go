package authenx

import (
	"errors"
	"fmt"
	"icebomb/icebomber/dbi"
	"icebomb/icebomber/helpers"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Prepares member data for external sign-in.
func MemberForExternalSignIn(c *gin.Context, email string, locale string, name string) (*dbi.Member, bool) {
	//conf := config.Get()
	//log := logger.Get().Logger
	db := dbi.Get().Db

	// checks if email is exists
	var member *dbi.Member
	result := db.Joins("Credential").First(&member, "email = ?", email)
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		// not found, creates it
		if locale != "en" && locale != "th" {
			locale = "en"
		}

		// create member
		generate := helpers.GenerateSecret(8)
		hash, salt := helpers.SetPassword(generate)
		uid := helpers.GenerateULID()
		member := &dbi.Member{
			Model:    gorm.Model{},
			ID:       uid,
			Email:    email,
			Role:     dbi.MemberRoleMember,
			Locale:   locale,
			Name:     name,
			Disabled: nil,
			Resigned: nil,
			Begin:    nil,
			End:      nil,
			Expire:   nil,
			Credential: dbi.MemberCredential{
				ID:    uid,
				Salt:  salt,
				Hash:  hash,
				Token: nil,
			},
		}

		// saves
		helpers.NewOrUpdateToken(member)

		// creates member
		db.Create(member)

		// adds log
		db.Create(&dbi.Logging{
			MemberId:    member.ID,
			Action:      "create",
			Table:       "member",
			Description: fmt.Sprintf("Member %v/%v is created via external sign-in.", member.ID, member.Email),
		})

		return member, true
	} else {
		// check if this member resigned or disabled
		if member.Resigned != nil {
			c.Error(helpers.ErrMemberIsResigned)
			return nil, false
		}
		if member.Disabled != nil {
			c.Error(helpers.ErrMemberIsDisabledByAdmin)
			return nil, false
		}

		// saves
		helpers.NewOrUpdateToken(member)

		// updates member
		db.Model(member).Updates(&dbi.Member{
			Begin:  member.Begin,
			End:    member.End,
			Expire: member.Expire,
		})
		db.Model(member.Credential).Update("token", member.Credential.Token)

		return member, false
	}

}
