package authenx

import (
	"fmt"
	"icebomb/icebomber/config"
	"icebomb/icebomber/dbi"
	"icebomb/icebomber/helpers"
	"icebomb/icebomber/logger"
	"net/http"
	"os"
	"path/filepath"

	"github.com/gin-gonic/gin"
	"github.com/go-resty/resty/v2"
)

type LineTokenType struct {
	AccessToken  string `json:"access_token"`
	ExpiresIn    int    `json:"expires_in"`
	RefreshToken string `json:"refresh_token"`
	Scope        string `json:"scope"`
	TokenType    string `json:"token_type"`
	IDToken      string `json:"id_token"`
}

type LineUserInformation struct {
	Email   string `json:"email"`
	Name    string `json:"name"`
	Picture string `json:"picture"`
}

// Sign-in using LINE.
func LineExternalSignIn(c *gin.Context) {
	conf := config.Get()
	log := logger.Get().Logger
	db := dbi.Get().Db
	id, _ := c.Get("id")

	client := resty.New()

	// extracts parameters
	code := c.DefaultQuery("code", "")

	// retrieves for token
	var token LineTokenType
	{
		resp, err := client.R().
			EnableTrace().
			SetResult(&token).
			SetHeader("Content-Type", "application/x-www-form-urlencoded").
			SetBody(fmt.Sprintf("code=%v&client_id=%v&client_secret=%v&redirect_uri=%v&grant_type=authorization_code",
				code, conf.LineClientId, conf.LineClientSecret, conf.LineRedirectUrl)).
			Post("https://api.line.me/oauth2/v2.1/token")
		if !helpers.RestyCheck(c, resp, err) {
			return
		}
		log.Printf("%v; Token retrieve: %v", id, logger.Dump(token))
	}

	// retrieves for member information
	var info LineUserInformation
	{
		resp, err := client.R().
			EnableTrace().
			SetResult(&info).
			SetHeader("Authorization", fmt.Sprintf("Bearer %v", token.IDToken)).
			Post(fmt.Sprintf("https://api.line.me/oauth2/v2.1/verify?id_token=%v&client_id=%v", token.IDToken, conf.LineClientId))
		if !helpers.RestyCheck(c, resp, err) {
			return
		}
		log.Printf("%v; Member info retrieve: %v", id, logger.Dump(info))
	}

	// creates or loads member
	member, created := MemberForExternalSignIn(c, info.Email, "en", info.Name)

	// if just created and have profile
	if created {
		// loads profile icon from host
		resp, err := client.R().
			Get(info.Picture)
		if !helpers.RestyCheck(c, resp, err) {
			return
		}

		// creates folder, if not exists
		cwd, _ := os.Getwd()
		dir := filepath.Join(cwd, conf.UploadDir, member.ID, "profile")
		os.MkdirAll(dir, os.ModePerm)
		log.Printf("%v; directory: %v", id, dir)

		// saves icon
		image := resp.Body()
		os.WriteFile(filepath.Join(dir, "icon"), image, 0666)

		// saves mime
		contentType := http.DetectContentType(image)
		os.WriteFile(filepath.Join(dir, "icon-mime"), []byte(contentType), 0666)
	}

	// adds log
	db.Create(&dbi.Logging{
		MemberId:    member.ID,
		Action:      "update",
		Table:       "member",
		Description: fmt.Sprintf("Member %v/%v is sign-in from LINE.", member.ID, member.Email),
	})

	c.JSON(http.StatusOK, gin.H{
		"member": member,
		"token":  member.Credential.Token,
	})
}
