package admin

import (
	"errors"
	"fmt"
	"icebomb/icebomber/dbi"
	"icebomb/icebomber/helpers"
	"icebomb/icebomber/logger"
	"icebomb/icebomber/routes/members"
	"net/http"
	"time"

	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

// Authorizes the member signing-up.  This service is primary used in testing only.
func AuthorizedMember(c *gin.Context) {
	//conf := config.Get()
	//log := logger.Get().Logger
	//db := dbi.Get().Db
	//id, _ := c.Get("id")
	//admin := helpers.AuthenGetMember(c)

	members.ConfirmSignUp(c)
}

// ----------------------------------------------------------------------

// Disabled DTO wrapper type.
type MemberDisabledWrapType struct {
	Member MemberDisabledType `json:"member"`
}

// Disabled DTO type.
type MemberDisabledType struct {
	Disabled bool `json:"disabled"`
}

// Disables or enables a member.
func DisabledMember(c *gin.Context) {
	//conf := config.Get()
	log := logger.Get().Logger
	db := dbi.Get().Db
	id, _ := c.Get("id")
	admin := helpers.AuthenGetMember(c)

	// binds body to JSON
	var json MemberDisabledWrapType
	if err := c.ShouldBindJSON(&json); err != nil {
		c.Error(helpers.ErrJSONSyntax)
		return
	}
	log.Printf("%v; JSON: %v", id, logger.Dump(json))
	if !helpers.Validate(c, json) {
		return
	}

	// selects member
	email := c.Param("email")
	var member *dbi.Member
	result := db.First(&member, "email = ?", email)
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		c.Error(helpers.ErrNotFound)
		return
	}

	// disabled or enabled
	if json.Member.Disabled {
		n := time.Now()
		member.Disabled = &n
	} else {
		member.Disabled = nil
	}
	db.Model(&member).Update("disabled", member.Disabled)

	// adds log
	db.Create(&dbi.Logging{
		MemberId:    member.ID,
		Action:      "update",
		Table:       "member",
		Description: fmt.Sprintf("Admin %v/%v changed %v/%v, disabled = %v.", admin.ID, admin.Email, member.ID, member.Email, json.Member.Disabled),
	})

	c.JSON(http.StatusOK, gin.H{
		"member": member,
	})
}
