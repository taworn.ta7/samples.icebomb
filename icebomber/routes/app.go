package routes

import (
	"icebomb/icebomber/config"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Returns server name and version.
func About(c *gin.Context) {
	conf := config.Get()
	c.JSON(http.StatusOK, gin.H{
		"app":     conf.AppName,
		"version": conf.AppVersion,
	})
}

// Returns current configuration.
func Config(c *gin.Context) {
	c.JSON(http.StatusOK, config.Get().ToMap())
}
