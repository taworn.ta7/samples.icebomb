// Package configuration for icebomber.
package config

import (
	"fmt"
	"strings"

	"github.com/spf13/viper"
	ordered "github.com/wk8/go-ordered-map"
)

// Configuration structure data type.
type ConfigType struct {
	AppName    string
	AppVersion string
	StorageDir string
	UploadDir  string

	// logging config
	LogFileName       string // file to output log, can use $... to expand variables
	LogMaxSize        int    // as megabytes
	LogMaxBackups     int    // number of files to keep
	LogMaxAge         int    // number of days to keep
	LogCompress       bool   // compress or not
	DaysToKeepDbLogs  int    // number of days to keep old database log
	DaysToKeepSignUps int    // number of days to keep old records in member_signup table
	DaysToKeepResets  int    // number of days to keep old records in member_reset table

	// database config
	DbUse  string // currently: sqlite or mysql
	DbHost string
	DbPort int
	DbUser string
	DbPass string
	DbName string // database name, in case of mysql
	DbFile string // database file, in case of sqlite

	// mail config
	MailHost  string
	MailPort  int
	MailUser  string
	MailPass  string
	MailAdmin string // administrator

	// HTTP config
	HttpPort int

	// authentication config
	AuthenTimeout int64 // time before session expire, as milli-seconds

	// profile config
	ProfileIconFileLimit int64 // number of bytes to limit upload profile icon

	// settings config
	AvailableSettings []string // list of all available key, please note that strings must be sorted!

	// Google config
	GoogleClientId     string
	GoogleClientSecret string
	GoogleRedirectUrl  string

	// LINE config
	LineClientId     string
	LineClientSecret string
	LineRedirectUrl  string
}

var config *ConfigType

// Retrieves the configuration.
func Get() *ConfigType {
	return config
}

// ----------------------------------------------------------------------

// Returns keys and their values.
func (c *ConfigType) ToOrderedMap() *ordered.OrderedMap {
	dict := ordered.New()
	dict.Set("AppName", c.AppName)
	dict.Set("AppVersion", c.AppVersion)
	dict.Set("StorageDir", c.StorageDir)
	dict.Set("UploadDir", c.UploadDir)
	dict.Set("LogFilename", c.LogFileName)
	dict.Set("LogMaxSize", c.LogMaxSize)
	dict.Set("LogMaxBackups", c.LogMaxBackups)
	dict.Set("LogMaxAge", c.LogMaxAge)
	dict.Set("LogCompress", c.LogCompress)
	dict.Set("DaysToKeepDbLogs", c.DaysToKeepDbLogs)
	dict.Set("DaysToKeepSignUps", c.DaysToKeepSignUps)
	dict.Set("DaysToKeepResets", c.DaysToKeepResets)
	dict.Set("DbUse", c.DbUse)
	dict.Set("DbHost", c.DbHost)
	dict.Set("DbPort", c.DbPort)
	dict.Set("DbUser", c.DbUser)
	dict.Set("DbName", c.DbName)
	dict.Set("DbFile", c.DbFile)
	dict.Set("MailHost", c.MailHost)
	dict.Set("MailPort", c.MailPort)
	dict.Set("MailUser", c.MailUser)
	dict.Set("MailAdmin", c.MailAdmin)
	dict.Set("HttpPort", c.HttpPort)
	dict.Set("AuthenTimeout", c.AuthenTimeout)
	dict.Set("ProfileIconFileLimit", c.ProfileIconFileLimit)
	dict.Set("AvailableSettings", c.AvailableSettings)
	dict.Set("GoogleClientId", c.GoogleClientId)
	dict.Set("GoogleRedirectUrl", c.GoogleRedirectUrl)
	dict.Set("LineClientId", c.LineClientId)
	dict.Set("LineRedirectUrl", c.LineRedirectUrl)
	return dict
}

// Returns keys and their values.
func (c *ConfigType) ToMap() *map[string]interface{} {
	dict := c.ToOrderedMap()
	result := make(map[string]interface{}, 0)
	for pair := dict.Oldest(); pair != nil; pair = pair.Next() {
		result[pair.Key.(string)] = pair.Value
	}
	return &result
}

// Prints out the configuration to string.
func (c *ConfigType) Log() string {
	builder := strings.Builder{}
	builder.WriteString("Configuration:\n")
	dict := c.ToOrderedMap()
	for pair := dict.Oldest(); pair != nil; pair = pair.Next() {
		builder.WriteString(fmt.Sprintf("  %v: %v\n", pair.Key, pair.Value))
	}
	return builder.String()
}

// ----------------------------------------------------------------------

// Setup the configuration.
func Setup() *ConfigType {
	{
		viper.SetConfigName("config")
		viper.AddConfigPath(".")
		err := viper.ReadInConfig()
		if err != nil {
			fmt.Printf("config.yaml is not parsed properly!\n")
			panic(err)
		}
	}

	{
		viper.SetConfigName("config.override")
		viper.AddConfigPath(".")
		err := viper.MergeInConfig()
		if err != nil {
			fmt.Printf("config.override.yaml is not parsed properly!\n")
			panic(err)
		}
	}

	c := &ConfigType{
		AppName:    viper.GetString("app.name"),
		AppVersion: viper.GetString("app.version"),
		StorageDir: viper.GetString("app.storageDir"),
		UploadDir:  viper.GetString("app.uploadDir"),

		LogFileName:       viper.GetString("log.fileName"),
		LogMaxSize:        viper.GetInt("log.maxSize"),
		LogMaxBackups:     viper.GetInt("log.maxBackups"),
		LogMaxAge:         viper.GetInt("log.maxAge"),
		LogCompress:       viper.GetBool("log.compress"),
		DaysToKeepDbLogs:  viper.GetInt("log.daysToKeepDbLogs"),
		DaysToKeepSignUps: viper.GetInt("log.daysToKeepSignUps"),
		DaysToKeepResets:  viper.GetInt("log.daysToKeepResets"),

		DbUse:  viper.GetString("db.use"),
		DbHost: viper.GetString("db.host"),
		DbPort: viper.GetInt("db.port"),
		DbUser: viper.GetString("db.user"),
		DbPass: viper.GetString("db.pass"),
		DbName: viper.GetString("db.name"),
		DbFile: viper.GetString("db.file"),

		MailHost:  viper.GetString("mail.host"),
		MailPort:  viper.GetInt("mail.port"),
		MailUser:  viper.GetString("mail.user"),
		MailPass:  viper.GetString("mail.pass"),
		MailAdmin: viper.GetString("mail.admin"),

		HttpPort: viper.GetInt("http.port"),

		AuthenTimeout: viper.GetInt64("authen.timeout"),

		ProfileIconFileLimit: viper.GetInt64("profile.iconFileLimit"),

		AvailableSettings: viper.GetStringSlice("settings.availableSettings"),

		GoogleClientId:     viper.GetString("google.clientId"),
		GoogleClientSecret: viper.GetString("google.clientSecret"),
		GoogleRedirectUrl:  viper.GetString("google.redirectUrl"),

		LineClientId:     viper.GetString("line.clientId"),
		LineClientSecret: viper.GetString("line.clientSecret"),
		LineRedirectUrl:  viper.GetString("line.redirectUrl"),
	}
	config = c
	return config
}
