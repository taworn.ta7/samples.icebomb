[สำหรับภาษาไทย คลิกที่นี่](./README-th.md)

# Icebomb

This is a REST server project.  Written by Go.

## Features

These samples demostration of features:

* Members system
* Send mail
* Delete unused records, configurable
* Upload profile icon
* Change language: English, Thai
* Change theme color
* Sign-in to Google and LINE, on web version only

## Screenshots

Screenshot pictures are in "%SOURCE%/samples.icebomb/screenshots".

## Installation

If you not installed development tools, installs them now:

* [Go](https://go.dev), for REST server
* [Vue](https://vuejs.org) and [modern web browser](https://www.google.com/chrome), for web client
* [Postman](https://www.postman.com), for testing server

## Run Server

Source code for REST server are in "samples.icebomb/icebomber".  The icebomber uses [GORM](https://gorm.io) as database.

Changes folder to:

	cd %SOURCE%/samples.icebomb/icebomber

Run:

	go run .

For the first run, uses REST client software, such as: Postman, to run:

	POST http://localhost:6600/api/setup

It will create member "admin@your.diy" with password "admin".

## Run Server Test

If you want to run test server, open Postman and import scripts "icebomber.postman_collection" and "icebomber.postman_environment" in folder "%SOURCE%/samples.icebomb/icebomber".  Don't forget to set Environments to "icebomber".

## Generate Server Document

If you want to generate document on REST server.  Type:

	cd %SOURCE%/samples.icebomb/icebomber

	godoc -http=:6060

and opens browser to URL:

	http://localhost:6060

Port number can be change.

## Run Web/App Client

Source code for web clients are in "samples.icebomb/icewebber" (no UI framework) or "samples.icebomb/iceweaver" (use bootstrap 3).

Changes folder to:

	cd %SOURCE%/samples.icebomb/icewebber
	-- or --
	cd %SOURCE%/samples.icebomb/iceweaver

Setup, one-time only:

	npm i

Run:

	npm start

Finally, opens browser to URL:

	http://localhost:8080

Please note at, as of writting date, bootstrap Vue 3 is in beta stage and have some components not implemented, yet.

Because the REST server is compatible with [Coldfire project](https://gitlab.com/taworn.ta7/samples.coldfire).  You can use both Flutter app and web clients in project Coldfire.  Download [Coldfire](https://gitlab.com/taworn.ta7/samples.coldfire) and see README on "Run App" or "Run Web".

## Configuration

There is a file called "config.override.yaml" in the root of server folder.  It keeps configuration and can be changes.

Here are some list:

log:
* daysToKeepDbLogs: number of days to keep old database log
* daysToKeepSignUps: number of days to keep old records in member_signup table
* daysToKeepResets: number of days to keep old records in member_reset table

db:
* use: mysql or sqlite, I think there are BUG in sqlite
* host: database host
* port: database port
* user: database user
* pass: database password

mail:
* host: mail sender host
* port: mail sender port
* user: mail sender user
* pass: mail sender password

authen:
* timeout: time before session expire, as milli-seconds

profile:
* iconFileLimit: number of bytes to limit upload profile icon

Note #1: If you use MySQL, don't forget to create blank database, named "icebomber".

Note #2: I used service [ethereal](https://ethereal.email), which is a fake mail service.
And, some time, your user and password will expire and you have to recreate them.  It's free.

## Programming Documents

There are more documents:

* [All APIs](./doc/APIs.md)
* [Error Handling for Client](./doc/Error%20Handling%20for%20Client.md)
* [Sort Ordering](./doc/Sort%20Ordering.md)

## Last

Sorry, but I'm not good at English. T_T
